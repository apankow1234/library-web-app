class Book {

    constructor( title, author, isbn, printingEdition=1 ) {
        this.title  = title;
        this.author = author;
        this.isbn   = isbn;
        // this.print  = printingEdition;
        let trigger = false;
        for( let i=0; i < Book.AllBooks.length; i++ ) {
          if( JSON.stringify( Book.AllBooks[i] ) === JSON.stringify( this ) ) {
            trigger = true;
          }
        }
        if( trigger == false ) {
            Book.AllBooks.push( this );
        }
    }

    get data() {
        return JSON.stringify( this );
    }

    isSameAs( otherBook ) {
        return this.data === otherBook.data;
    }

    static FindBooksBy( search, library, type=0 ) {
        library = ( library == undefined || library == null ) ? Book.AllBooks : library;
        let matches= [];
        for( let i=0; i<library.length; i++ ) {
            let match = false;
            if( type == 0 ) {        // Title
                match = ( library[i].title  == search ) ? true : false;
            } else if( type == 1 ) { // Author
                match = ( library[i].author == search ) ? true : false;
            } else if( type == 2 ) { // ISBN#
                match = ( library[i].isbn   == search ) ? true : false;
            }
            if( match || search == "\*" ) {
                matches.push( library[i] );
            }
        }
        return matches;
    }

    static FindBooks( search, library ) {
        let allMatches = [];
        let matches = [];
        for( let i=0; i<3; i++ ) {
            matches = Book.FindBooksBy( search, library, type=i );
            if( matches != [] && matches != undefined ) {
                allMatches.concat( matches );
            }
        }
        return allMatches;
    }

    static FindBooksByTitle(     title,  library ) {
        return Book.FindBooksBy( title,  library, type=0 );
    }
    static FindBooksByAuthor(    author, library ) {
        return Book.FindBooksBy( author, library, type=1 );
    }
    static FindBooksByISBN(      isbn,   library ) {
        return Book.FindBooksBy( isbn,   library, type=2 );
    }

}
Book.AllBooks = [];