const defaultBooks = [

];

// const defaultBooks = [
//     new Book( "Story 1",           "Jon Doe",          100012345 ),
//     new Book( "Harry Potter and the Sorcerer's Stone", "J.K. Rowling", 9781781100486 ),
//     new Book( "Scary Story 1",     "Horror Author 1",  984612100 ),
//     new Book( "Ooh la la Story 1", "Romance Author 1", 234516089 ),
//     new Book( "Project Book 1",    "DIY Author 1",     494007840 )
// ];
// LocalDB.DeleteAll( 'books' );
// defaultBooks.forEach( ( book ) => LocalDB.Create( 'books', book ) );

document.addEventListener( 'DOMContentLoaded', () => {
    let allDBTableAreas = document.querySelectorAll( '.database-view' );
    for( let i=0; i < allDBTableAreas.length; i++ ) {
        let data = allDBTableAreas[i].dataset;
        let toggleTo = ( data.editable == "true" );
        new DatabaseView( data.key, allDBTableAreas[i] ).toggle( toggleTo );
    }
    let allDBFormAreas = document.querySelectorAll( '.database-form' );
    for( let i=0; i < allDBFormAreas.length; i++ ) {
        let data = allDBFormAreas[i].dataset;
        let formType = data.type;
        new DatabaseAddForm( data.key, allDBFormAreas[i] );
    }
    DatabaseView.RenderAll();
    DatabaseAddForm.RenderAll();


    function Prompt( inputElement ) {
        let prompter = document.createElement('div');
        prompter.id = "Autofill";
        let prefix = inputElement.id.match(/^\w\S*\-/)[0].length;
        let suffix = inputElement.id.match( /\d*$/ ).index;
        let dataType = inputElement.id.substring( prefix, suffix );
        prompter.dataset['type'] = dataType;
        inputElement.parentElement.insertBefore( prompter, inputElement.nextElementSibling);
    }
    function UnPrompt() {
        let el = document.querySelector( "#Autofill" );
        el.parentElement.removeChild( el );
    }

    function Search( inputElement ) {
        let type = inputElement.nextElementSibling.dataset['type'];
        if( type == 'isbn' ) {
            searchType = 0;
        } else if( type == 'title' ) {
            searchType = 1;
        } else if( type == 'author' ) {
            searchType = 2;
        }
        // console.log( 'type', type, searchType );
        inputElement.nextElementSibling.innerHTML = ``;
        BookDB.GetBooks( inputElement.value, (book) => {
            let entry = { 
                "title"       : (book["title"])
                , "author"   : (book["authors"])
                , "isbn"      : (book["industryIdentifiers"][0]['identifier']) 
                // , "printType: " : (book["printType"])
            }
            let row = document.createElement('p');
            // console.log( type, searchType, entry[ type ] );
            if( entry[ type ] ) {
                row.innerText += entry[ type ];
                inputElement.nextElementSibling.appendChild( row );
            }
        }, searchType )
    }

    // let inputElements = document.querySelectorAll("input");
    // for( let i=0; i < inputElements.length; i++ ) {
    //     inputElements[i].addEventListener( 'focus', (e) => {
    //         Prompt( e.target );
    //     } );
    //     inputElements[i].addEventListener( 'blur', () => {
    //         UnPrompt();
    //     } );
    //     inputElements[i].addEventListener( 'input', (e) => {
    //         Search( e.target );
    //     } );
    // }
} );