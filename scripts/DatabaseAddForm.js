class DatabaseAddForm {
    constructor( key, container ) {
        this.container = container;
        this.key       = key;
        this.records   = LocalDB.Retrieve( this.key );
        this.fields    = Object.keys( this.records[0] );
        this.columns   = this.fields.length;

        this.form;
        DatabaseAddForm.AllForms.push( this );
    }

    GetInputs() {
        let record = {};
        let inputs = this.form.querySelectorAll('input');
        for( let i=0; i < this.columns; i++ ) {
            let match = new RegExp( this.fields[i] );
            if( (inputs[i].id.match( match )) ) {
                record[ this.fields[i].toLocaleLowerCase() ] = inputs[i].value;
            }
        }
        return record;
    }

    Render() {
        this.form = document.createElement('form');
        this.form.addEventListener( 'submit', (e) => {
            e.preventDefault();

            LocalDB.Create( this.key, this.GetInputs() );
            DatabaseView.RenderAll();
            DatabaseAddForm.RenderAll();
        });

        for( let f=0; f < this.columns; f++ ) {
            let label = document.createElement( 'label' );
            label.innerHTML = this.fields[f];
            let input = document.createElement( 'input' );
            input.type = "text";
            input.id = "add-" + this.fields[f] + DatabaseAddForm.AllForms.length;
            label.setAttribute('for', input.id);
            this.form.appendChild( label );
            this.form.appendChild( input );
        }
        let submit = document.createElement( 'button' );
        submit.type = "submit";
        submit.innerText = "add " + this.key;
        this.form.appendChild( submit );
        submit.id = "add-submit" + DatabaseAddForm.AllForms.length;
        this.container.innerHTML = ``;
        this.container.appendChild( this.form );
        return this;
    }
    
    static RenderAll() {
        DatabaseAddForm.AllForms.forEach( (f) => f.Render() );
    }
}
DatabaseAddForm.AllForms = [];