class DatabaseView {
    constructor( key, container ) {
        this.container = container;
        this.key       = key;
        this.records   = LocalDB.Retrieve(  key  );
        this.headers   = Object.keys( this.records[0] );
        this.columns   = this.headers.length;
        this.editable  = false;

        this.thead;
        this.tbody;
        DatabaseView.AllTables.push( this );
    }

    toggle( toggleTo ) {
        if( toggleTo != undefined && toggleTo != null )
            this.editable = ( toggleTo == false || toggleTo == 0 ) ? false : true;
        else
            this.editable = ( this.editable ) ? false : true;
        return this;
    }

    Render() {
        this.records   = LocalDB.Retrieve(  this.key  );
        let table = document.createElement('table');
        table.classList.add('database');
        this.thead = document.createElement('thead');
        let hrow  = document.createElement('tr');
        this.tbody = document.createElement('tbody');
        table.appendChild( this.thead );
        this.thead.appendChild( hrow  );
        table.appendChild( this.tbody );

        for( let h=0; h < this.columns; h++ ) {
            hrow.innerHTML += `<th>${ this.headers[h] }</th>`;
        }
        if( this.editable ) {
            hrow.innerHTML += `<th>&nbsp;</th>`;
        }
        for( let r=0; r < this.records.length; r++ ) {
            let drow  = document.createElement('tr');
            for( let d=0; d < this.columns; d++ ) {
                drow.innerHTML += `<td>${ this.records[r][ this.headers[d] ] }</td>`;
            }
            if( this.editable ) {
                let cell = document.createElement( 'td' );
                let delButton = document.createElement( 'button' );
                delButton.classList.add( 'remove' );
                delButton.innerText = "X";
                delButton.addEventListener( 'click', (e) => {
                    e.preventDefault();
                    let row = e.target.closest( 'tr' );
                    let idx = [ ...this.tbody.children ].indexOf( row );
                    LocalDB.Delete( this.key, this.records[ idx ] );
                    this.tbody.removeChild( row );
                });
                cell.appendChild( delButton );
                drow.appendChild( cell );
            }
            this.tbody.appendChild( drow  );
        }
        this.container.innerHTML = ``;
        this.container.appendChild( table );
        return this;
    }
    
    static RenderAll() {
        DatabaseView.AllTables.forEach( (t) => t.Render() );
    }

}
DatabaseView.AllTables = [];