class BookDB {

    static FetchBookData( url, cbFunction ) {
        fetch( url, { method : 'get' } )
        .then( response => response.json() )
        .then( data => {
            let books = [];
            BookDB.LastSearch = [];
            for( let i=0; i < data.items.length; i++ ) {
                let book = data.items[i].volumeInfo;
                BookDB.LastSearch.push( book );
                cbFunction( book );
            }
        })
        .catch( error => console.error(error) );
    }

    static GetBooks( input, cbFunction, type=0 ) {
        if( type == 0 ) {
            BookDB.GetBooksDetailsByISBN( input, cbFunction );
        } else if( type == 1 ) {
            BookDB.GetBooksDetailsByTitle( input, cbFunction );
        } else if( type == 2 ) {
            BookDB.GetBooksDetailsByAuthor( input, cbFunction );
        }
    }

    static GetBooksDetailsByISBN( isbn = "9780671039738", cbFunction ) {
        var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:"   + isbn;
        BookDB.FetchBookData( url, cbFunction );
      }

      static GetBooksDetailsByTitle( title, cbFunction ) {
        var url = "https://www.googleapis.com/books/v1/volumes?q=title:"  + title;
        BookDB.FetchBookData( url, cbFunction );
    }

    static GetBooksDetailsByAuthor( author, cbFunction ) {
        var url = "https://www.googleapis.com/books/v1/volumes?q=author:" + author;
        BookDB.FetchBookData( url, cbFunction );
    }

}

BookDB.LastSearch = [];