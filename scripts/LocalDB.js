class LocalDB {

  static Create( key, value ) {
    if( value != undefined && value != null && value != "" && value != [] && value != {} ) {
      let records = LocalDB.Retrieve( key );
      let trigger = false;
      for( let i=0; i < records.length; i++ ) {
        if( JSON.stringify( records[i] ) === JSON.stringify( value ) ) {
          trigger = true;
        }
      }
      if( trigger == false ) {
        records.push( value );
      }
      localStorage.setItem( key, JSON.stringify( records ) );
    }
  }

  static Retrieve( key ) {
    let records;
    if( localStorage.getItem( key ) === null ) {
      records = [];
    } else {
      records = JSON.parse( localStorage.getItem( key ) );
    }
    return records;
  }

  static Delete( key, value ) {
    let records = LocalDB.Retrieve( key );
    for( let i=0; i < records.length; i++ ) {
      if( JSON.stringify( records[i] ) === JSON.stringify( value ) ) {
        records.splice( i--, 1 );
      }
    };
    localStorage.setItem( key, JSON.stringify( records ) );
  }

  static DeleteAll( key ) {
    localStorage.setItem( key, JSON.stringify( [] ) );
  }

}